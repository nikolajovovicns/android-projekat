package com.example.androidprojekat;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.androidprojekat.Adapter.CartListAdapter;
import com.example.androidprojekat.Helper.MenagmentCart;
import com.example.androidprojekat.Interface.ChangeNumberItemsListener;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

public class CartListActivity extends AppCompatActivity {

    // // Adapter, RecyclerView, MenagmentCart, TextView-i, ScrollView i promjenjiva za izračunavanje poreza su privatni članovi klase koji su potrebni za prikaz i manipulaciju podacima korpe.
    private RecyclerView.Adapter adapter;
    private RecyclerView recyclerViewList;
    private MenagmentCart menagmentCart;
    TextView totalItemsTxt, taxCartTxt, deliveryCartTxt, totalCartTxt, emptyCartTxt, obrisiDugme;
    private double taxCart;
    private ScrollView scrollView;

    // onCreate() metoda se poziva pri pokretanju aktivnosti koja prikazuje korpu i poziva metode za inicijalizaciju view-a, postavljanje liste proizvoda u korpi, izračunavanje ukupne cene korpe i postavljanje donje navigacije.
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cart_list);

        menagmentCart = new MenagmentCart(this);

        initView();
        initList();
        CalculatorCart();
        bottomNavigation();
        deleteBase();
    }

    // Ovaj metod implementira donju funkcionalnost navigacije u aplikaciji koristeći dve interakcije korisnika i anonimnu klasu za registraciju slušalaca događaja.
    public void bottomNavigation() {
        FloatingActionButton floatingActionButton = findViewById(R.id.cartBtn);
        LinearLayout homeBtn = findViewById(R.id.homeBtn);

        floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(CartListActivity.this, CartListActivity.class));
            }
        });

        homeBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(CartListActivity.this, MainActivity.class));
            }
        });
    }

    // Ova metoda implementira brisanje svih stavki sa kartice kada se pritisne na dugme "Check Out" i vraca na stranicu za dodavanje u korpu
    private void deleteBase(){
        obrisiDugme.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                menagmentCart.removeFromCart();
                startActivity(new Intent(CartListActivity.this, MainActivity.class));
            }
        });
    }

    // initView() metoda se koristi za inicijalizaciju svih View elemenata koji su neophodni za prikaz korpe, uključujući RecyclerView, TextView-ove koji prikazuju ukupan broj proizvoda, porez, dostavu, dugme koje brise sve stavke sa kartice i ukupnu cenu korpe
    private void initView() {
        recyclerViewList = findViewById(R.id.recyclerView);
        totalItemsTxt = findViewById(R.id.itemsTotalCart);
        taxCartTxt = findViewById(R.id.taxCart);
        deliveryCartTxt = findViewById(R.id.deliveryServiceCart);
        totalCartTxt = findViewById(R.id.totalCart);
        emptyCartTxt = findViewById(R.id.emptyCart);
        scrollView = findViewById(R.id.scrollViewCart);
        recyclerViewList = findViewById(R.id.recylerViewCart);
        obrisiDugme = findViewById(R.id.deleteBtn);
    }

    // initList() metoda postavlja LinearLayoutManager i CartListAdapter za RecyclerView koji se koristi za prikaz proizvoda u korpi
    // Takođe proverava da li je korpa prazna i ako jeste, prikazuje TextView sa odgovarajućom porukom, a ako nije, prikazuje RecyclerView
    private void initList() {
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        recyclerViewList.setLayoutManager(linearLayoutManager);
        adapter = new CartListAdapter(menagmentCart.getListCart(), this, new ChangeNumberItemsListener() {
            @Override
            public void changed() {
                CalculatorCart();
            }
        });

        recyclerViewList.setAdapter(adapter);
        if(menagmentCart.getListCart().isEmpty()) {
            emptyCartTxt.setVisibility(View.VISIBLE);
            scrollView.setVisibility(View.GONE);
        }else{
            emptyCartTxt.setVisibility(View.GONE);
            scrollView.setVisibility(View.VISIBLE);
        }
    }

    // CalculatorCart() metoda se koristi za izračunavanje ukupne cene korpe.
    // Izračunava se porez i ukupna cena korpe, a zatim se postavljaju vrednosti TextView-ova koji prikazuju te podatke.
    private void CalculatorCart() {
        double precentTax = 0.02;
        double delivery = 10;

        double tax = Math.round((menagmentCart.getTotalFee() * precentTax) + 100) / 100;
        double itemTotal = Math.round(menagmentCart.getTotalFee() * 100) / 100;
        double total = Math.round((menagmentCart.getTotalFee() + tax + delivery) * 100) / 100;

        totalItemsTxt.setText("$" + itemTotal);
        deliveryCartTxt.setText("$" +delivery);
        taxCartTxt.setText("$" + tax);
        totalCartTxt.setText("$" + total);
    }
}