package com.example.androidprojekat;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.androidprojekat.Domain.FoodDomain;
import com.example.androidprojekat.Helper.MenagmentCart;

import org.w3c.dom.Text;

public class ShowDetailActivity extends AppCompatActivity {

    // Deklarišemo nekoliko TextView, ImageView i FoodDomain objekat koji se koriste za prikazivanje detalja hrane ili proizvoda, kao i MenagmentCart objekat koji se koristi za upravljanje korpe sa porudžbinama.
    private TextView addToCartBtn;
    private TextView titleTxt, feeTxt, descriptionTxt, numberOrderTxt;
    private ImageView plusBtn, minusBtn, picFood;
    private FoodDomain object;

    int numberOrder = 1;
    private MenagmentCart menagmentCart;

    // onCreate() metoda se poziva kada se aktivnost ShowDetailActivity kreira i postavlja potrebne elemente korisničkog interfejsa i dobavlja podatke iz paketa prethodne aktivnosti.
    // Takođe inicijalizuje menadžer korpe i prikazuje informacije o hrani u korisničkom interfejsu.
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_detail);

        menagmentCart = new MenagmentCart(this);
        initView();
        getBundle();
    }

    // getBundle() metoda koristi getIntent().getSerializableExtra() za dobijanje FoodDomain objekta koji je poslat kao dodatni podatak iz prethodne aktivnosti.
    // Zatim se postavlja slika hrane pomoću Glide biblioteke.
    // TitleTxt, feeTxt, descriptionTxt i numberOrderTxt su postavljeni na odgovarajuće vrednosti objekta.
    // PlusBtn i minusBtn služe za povećanje i smanjenje broja hrane u korpi.
    // Na kraju, addToCartBtn postavlja numberInCart atribut objekta i dodaje ga u korpu putem MenagmentCart klase.
    private void getBundle() {
        object = (FoodDomain) getIntent().getSerializableExtra("object");
        int drawableResourceId = this.getResources().getIdentifier(object.getPic(), "drawable",this.getPackageName());

        Glide.with(this)
                .load(drawableResourceId)
                .into(picFood);

        titleTxt.setText(object.getTitle());
        feeTxt.setText("$"+object.getFee());
        descriptionTxt.setText(object.getDescription());
        numberOrderTxt.setText(String.valueOf(numberOrder));

        plusBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                numberOrder = numberOrder+1;
                numberOrderTxt.setText(String.valueOf(numberOrder));
            }
        });

        minusBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (numberOrder>1)
                {
                    numberOrder = numberOrder-1;
                }
                else
                {
                    numberOrderTxt.setText(String.valueOf(numberOrder));
                }
            }
        });

        addToCartBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                object.setNumberInCart(numberOrder);
                menagmentCart.insertFood(object);
            }
        });
    }

    // initView() metoda se koristi za inicijalizaciju svih View elemenata koji su neophodni za prikaz pojedinačnog jela u meniju, uključujući dugme za dodavanje u korpu, naslov, cenu, opis, broj narudžbi, dugmad za povećanje i smanjenje broja narudžbi i sliku hrane.
    private void initView () {
        addToCartBtn = findViewById(R.id.addToCartBtn);
        titleTxt = findViewById(R.id.titleTxt);
        feeTxt = findViewById(R.id.priceTxt);
        descriptionTxt = findViewById(R.id.descriptionTxt);
        numberOrderTxt = findViewById(R.id.numberOrderTxt);
        plusBtn = findViewById(R.id.plusBtn);
        minusBtn = findViewById(R.id.minusBtn);
        picFood = findViewById(R.id.picFood);
    }
}