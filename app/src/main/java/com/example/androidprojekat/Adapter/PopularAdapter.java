package com.example.androidprojekat.Adapter;

import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.androidprojekat.Domain.FoodDomain;
import com.example.androidprojekat.R;
import com.example.androidprojekat.ShowDetailActivity;

import org.w3c.dom.Text;

import java.util.ArrayList;

// Ovaj kod predstavlja implementaciju adaptera za RecyclerView.
// Adapter služi kao veza između podataka koje prikazujemo i RecyclerView-a koji prikazuje ove podatke na ekranu.

public class PopularAdapter extends RecyclerView.Adapter<PopularAdapter.ViewHolder> {

    ArrayList<FoodDomain> popularFood;

    // Metoda public PopularAdapter(ArrayList<FoodDomain> categoryFood) služi za inicijalizaciju adaptera i postavljanje podataka koji će biti prikazani.
    public PopularAdapter(ArrayList<FoodDomain> categoryFood) {
        this.popularFood = categoryFood;
    }

    // Metoda onCreateViewHolder se poziva kada se treba kreirati novi ViewHolder objekat, koji će držati referencu na prikaz jedne stavke u RecyclerView-u.
    @Override
    public PopularAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View inflate = LayoutInflater.from(parent.getContext()).inflate(R.layout.viewholder_popular,parent, false);

        return new ViewHolder(inflate);
    }

    // Kod u metodi onBindViewHolder postavlja tekst, sliku i dodaje listener za dugme za svaku stavku u RecyclerView-u.
    // Ovaj listener će pokrenuti ShowDetailActivity kada se klikne na dugme i proslediće odgovarajući objekat sa podacima u intent-u.
    @Override
    public void onBindViewHolder(@NonNull PopularAdapter.ViewHolder holder, int position) {
        holder.title.setText(popularFood.get(position).getTitle());
        holder.fee.setText(String.valueOf(popularFood.get(position).getFee()));

        int drawableResourceId = holder.itemView.getContext().getResources().getIdentifier(popularFood.get(position).getPic(),"drawable", holder.itemView.getContext().getPackageName());

        Glide.with(holder.itemView.getContext())
                .load(drawableResourceId)
                .into(holder.pic);

        holder.addBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(holder.itemView.getContext(), ShowDetailActivity.class);
                intent.putExtra("object", popularFood.get(position));
                holder.itemView.getContext().startActivity(intent);
            }
        });
    }

    // Metoda getItemCount vraća broj elemenata koji će biti prikazani u RecyclerView-u.
    @Override
    public int getItemCount() {
        return popularFood.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView title,fee;
        ImageView pic;
        TextView addBtn;


        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            title = itemView.findViewById(R.id.title);
            fee = itemView.findViewById(R.id.fee);
            pic = itemView.findViewById(R.id.pic);
            addBtn = itemView.findViewById(R.id.addBtn);
        }
    }
}