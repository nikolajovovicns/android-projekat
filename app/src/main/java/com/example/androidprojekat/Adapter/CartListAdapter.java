package com.example.androidprojekat.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.androidprojekat.Domain.FoodDomain;
import com.example.androidprojekat.Helper.MenagmentCart;
import com.example.androidprojekat.Interface.ChangeNumberItemsListener;
import com.example.androidprojekat.R;

import java.util.ArrayList;

// Ova klasa implementira adapter koji prikazuje stavke u korpi i omogućava korisniku da ih dodaje i uklanja.
public class CartListAdapter extends RecyclerView.Adapter<CartListAdapter.ViewHolder> {

    // foodDomains je lista objekata tipa FoodDomain koja sadrži stavke hrane koje se prikazuju u korpi.
    // menagmentCart je objekat klase MenagmentCart koji se koristi za dodavanje i uklanjanje stavki iz korpe.
    // changeNumberItemsListener je listener koji se poziva kada se broj stavki u korpi promeni.
    private ArrayList<FoodDomain> foodDomains;
    private MenagmentCart menagmentCart;
    private ChangeNumberItemsListener changeNumberItemsListener;

    // Metoda CartListAdapter kreira novu instancu adaptera. Ova metoda prima tri argumenta: listu stavki hrane, kontekst aplikacije i listener koji se poziva kada se broj stavki u korpi promeni.
    public CartListAdapter(ArrayList<FoodDomain> foodDomains, Context context, ChangeNumberItemsListener changeNumberItemsListener) {
        this.foodDomains = foodDomains;
        this.menagmentCart = new MenagmentCart(context);
        this.changeNumberItemsListener = changeNumberItemsListener;
    }

    // Metoda onCreateViewHolder kreira novi ViewHolder objekat koji predstavlja prikaz jedne stavke u korpi.
    // Ova metoda prima dva argumenta: roditeljsku ViewGroup i tip prikaza.
    // U ovoj metodi se učitava layout fajl viewholder_cart.xml koji predstavlja prikaz jedne stavke u korpi.
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View inflate = LayoutInflater.from(parent.getContext()).inflate(R.layout.viewholder_cart, parent,false);
        return new ViewHolder(inflate);
    }

    // onBindViewHolder popunjava podatke ViewHolder objekta koji se nalazi na određenoj poziciji u RecyclerView-u.
    // Ova metoda prikazuje naziv stavke, cenu po komadu, ukupnu cenu stavke u korpi, sliku i broj komada u korpi.
    // Takođe, ova metoda postavlja i listenere za dugmad za dodavanje i uklanjanje stavki iz korpe.
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.title.setText(foodDomains.get(position).getTitle());
        holder.feeEachItem.setText(String.valueOf(foodDomains.get(position).getFee()));
        holder.totalEachItem.setText(String.valueOf(Math.round((foodDomains.get(position).getNumberInCart() * foodDomains.get(position).getFee()) * 100) / 100));
        holder.num.setText(String.valueOf(foodDomains.get(position).getNumberInCart()));

        int drawableResourceId = holder.itemView.getContext().getResources().getIdentifier(foodDomains.get(position).getPic(), "drawable", holder.itemView.getContext().getPackageName());

        Glide.with(holder.itemView.getContext())
                .load(drawableResourceId)
                .into(holder.pic);

        holder.plusItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                menagmentCart.plusNumberFood(foodDomains, position, new ChangeNumberItemsListener() {
                    @Override
                    public void changed() {
                        notifyDataSetChanged();
                        changeNumberItemsListener.changed();
                    }
                });
            }
        });

        holder.minusItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                menagmentCart.minusNumberFood(foodDomains, position, new ChangeNumberItemsListener() {
                    @Override
                    public void changed() {
                        notifyDataSetChanged();
                        changeNumberItemsListener.changed();
                    }
                });
            }
        });
    }

    // Metoda getItemCount vraća broj stavki u korpi.
    @Override
    public int getItemCount() {
        return foodDomains.size();
    }


    // Unutar klase ViewHolder se inicijalizuju svi elementi koji se nalaze u prikazu jedne stavke u korpi, kao što su naziv, slika, cena, broj komada i dugmad za dodavanje i uklanjanje stavki
    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView title, feeEachItem;
        ImageView pic,plusItem, minusItem;
        TextView totalEachItem, num;


        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            title = itemView.findViewById(R.id.titleCartTxt);
            feeEachItem = itemView.findViewById(R.id.feeEachItemCart);
            pic = itemView.findViewById(R.id.picEachCart);
            totalEachItem = itemView.findViewById(R.id.totalEachItemCart);
            num = itemView.findViewById(R.id.numberItemCart);
            plusItem = itemView.findViewById(R.id.plusCartBtn);
            minusItem = itemView.findViewById(R.id.minusCartBtn);
        }
    }
}