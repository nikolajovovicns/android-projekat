package com.example.androidprojekat.Adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.androidprojekat.Domain.CategoryDomain;
import com.example.androidprojekat.R;

import java.util.ArrayList;

public class CategoryAdapter extends RecyclerView.Adapter<CategoryAdapter.ViewHolder> {

    ArrayList<CategoryDomain> categoryDomains;

    // Metoda CategoryAdapter je konstruktor koji prima listu kategorija kao argument i postavlja je na odgovarajuće polje.
    public CategoryAdapter(ArrayList<CategoryDomain> categoryDomains) {
        this.categoryDomains = categoryDomains;
    }

    // Metoda "onCreateViewHolder" kreira novi ViewHolder objekt za svaki element liste, tj. za svaku kategoriju.
    // Metoda inflate-uje prikaz (view) ViewHolder-a koristeći "LayoutInflater" klasu i vraća novi ViewHolder objekt koji sadrži taj prikaz.
    @Override
    public CategoryAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View inflate = LayoutInflater.from(parent.getContext()).inflate(R.layout.viewholder_category,parent, false);

        return new ViewHolder(inflate);
    }

    // Metoda "onBindViewHolder" prikazuje podatke o kategoriji u odgovarajućem ViewHolder-u.
    // Postavlja se naziv kategorije na TextView element, a pozadina ViewHolder-a se postavlja na osnovu pozicije u listi kategorija.
    // Uz to, učitava se slika za kategoriju koristeći "Glide" biblioteku i postavlja se na ImageView element.
    @Override
    public void onBindViewHolder(@NonNull CategoryAdapter.ViewHolder holder, int position) {
        holder.categoryName.setText(categoryDomains.get(position).getTitle());
        String picUrl = "";
        switch (position){
            case 0:{
                picUrl = "cat_1";
                holder.mainLayout.setBackground(ContextCompat.getDrawable(holder.itemView.getContext(),R.drawable.cat_background1));
                break;
            }
            case 1:{
                picUrl = "cat_2";
                holder.mainLayout.setBackground(ContextCompat.getDrawable(holder.itemView.getContext(),R.drawable.cat_background2));
                break;
            }
            case 2:{
                picUrl = "cat_3";
                holder.mainLayout.setBackground(ContextCompat.getDrawable(holder.itemView.getContext(),R.drawable.cat_background3));
                break;
            }
            case 3:{
                picUrl = "cat_4";
                holder.mainLayout.setBackground(ContextCompat.getDrawable(holder.itemView.getContext(),R.drawable.cat_background4));
                break;
            }
            case 4:{
                picUrl = "cat_5";
                holder.mainLayout.setBackground(ContextCompat.getDrawable(holder.itemView.getContext(),R.drawable.cat_background5));
                break;
            }
        }
        int drawableResourceId = holder.itemView.getContext().getResources().getIdentifier(picUrl,"drawable", holder.itemView.getContext().getPackageName());

        Glide.with(holder.itemView.getContext())
                .load(drawableResourceId)
                .into(holder.categoryPic);
    }

    // Metoda "getItemCount" vraća broj kategorija koje se prikazuju u RecyclerView-u.
    @Override
    public int getItemCount() {
        return categoryDomains.size();
    }

    // Klasa ViewHolder definira elemente prikaza ViewHolder-a, tj. T
    // extView za naziv kategorije, ImageView za prikaz slike kategorije i ConstraintLayout koji se koristi za pozadinu ViewHolder-a.
    // Konstruktor ViewHolder klase prima View objekt, a zatim inicijalizuje gore pomenute elemente prikaza pomoću "findViewById" metoda.
    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView categoryName;
        ImageView categoryPic;
        ConstraintLayout mainLayout;


        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            categoryName = itemView.findViewById(R.id.categoryName);
            categoryPic = itemView.findViewById(R.id.categoryPic);
            mainLayout =  itemView.findViewById(R.id.mainLayout);
        }
    }
}