package com.example.androidprojekat.Interface;


// Definišemo interfejs koji u sebi ima samo jednu metodu changed()

public interface ChangeNumberItemsListener {
    void changed();
}
