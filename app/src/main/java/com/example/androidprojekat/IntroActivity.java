package com.example.androidprojekat;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;


public class IntroActivity extends AppCompatActivity {
    // Deklarišemo ConstraintLayout objekat koji se koristi za postavljanje klikabilnog dugmeta koje će preći na glavnu aktivnost aplikacije.
    private ConstraintLayout startBtn;

    //Ova metoda se poziva prilikom kreiranja aktivnosti "IntroActivity".
    // U ovoj metodi postavljamo layout za ovu aktivnost i inicijalizujemo dugme "startBtn".
    // Kada korisnik pritisne na ovo dugme, otvara se nova aktivnost "MainActivity" preko koje korisnik može da koristi funkcionalnosti aplikacije.
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_intro);

        startBtn = findViewById(R.id.startBtn);
        startBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(IntroActivity.this,MainActivity.class));
            }
        });
    }
}
