package com.example.androidprojekat.Helper;

import android.content.Context;
import android.widget.Toast;

import com.example.androidprojekat.Domain.FoodDomain;
import com.example.androidprojekat.Interface.ChangeNumberItemsListener;

import java.util.ArrayList;

public class MenagmentCart {
    private Context context;
    private TinyDB tinyDB;

    // Konstruktor klase koja inicijalizuje kontekst i instancu klase TinyDB za čuvanje liste proizvoda u lokalnoj memoriji.
    public MenagmentCart(Context context) {
        this.context = context;
        this.tinyDB = new TinyDB(context);
    }

    // Metoda "insertFood" dodaje novi proizvod u listu, proverava da li već postoji u listi i ažurira količinu ako postoji.
    // Na kraju ažurira listu proizvoda u lokalnoj memoriji i prikazuje Toast poruku o dodavanju proizvoda na karticu.
    public void insertFood(FoodDomain item)
    {
        ArrayList<FoodDomain> listFood = getListCart();
        boolean existAlredy = false;
        int n = 0;
        for(int i = 0;i<listFood.size();i++)
        {
            if (listFood.get(i).getTitle().equals(item.getTitle())) {
                existAlredy = true;
                n = 1;
                break;
            }
        }

        if(existAlredy) {
            listFood.get(n).setNumberInCart(item.getNumberInCart());
        }
        else
        {
            listFood.add(item);
        }

        tinyDB.putListObject("CartList", listFood);
        Toast.makeText(context,"Proizvod je dodan na karticu",Toast.LENGTH_SHORT).show();
    }

    // Metoda "getListCart" vraća listu proizvoda sa kartice koja je sačuvana u lokalnoj memoriji.
    public ArrayList<FoodDomain> getListCart() {
        return  tinyDB.getListObject("CartList");
    }

    // Metoda "plusNumberFood" povećava količinu proizvoda u listi za jedan na datoj poziciji, ažurira listu u lokalnoj memoriji i poziva metodu ChangeNumberItemsListener.changed().
    public void plusNumberFood(ArrayList<FoodDomain> listFood, int positon, ChangeNumberItemsListener changeNumberItemsListener)
    {
        listFood.get(positon).setNumberInCart(listFood.get(positon).getNumberInCart() + 1);
        tinyDB.putListObject("CartList", listFood);

        changeNumberItemsListener.changed();
    }

    // Metoda "minusNumberFood" smanjuje količinu proizvoda u listi za jedan na datoj poziciji, uklanja proizvod iz liste ako je količina jednaka 1 i ažurira listu u lokalnoj memoriji.
    // Na kraju poziva metodu ChangeNumberItemsListener.changed().
    public void minusNumberFood(ArrayList<FoodDomain> listFood, int positon, ChangeNumberItemsListener changeNumberItemsListener)
    {
        if (listFood.get(positon).getNumberInCart() == 1)
        {
            listFood.remove(positon);
        }
        else
        {
            listFood.get(positon).setNumberInCart(listFood.get(positon).getNumberInCart() - 1);
        }
        tinyDB.putListObject("CartList", listFood);
        changeNumberItemsListener.changed();
    }

    // Metoda "getTotalFee" vraća ukupan iznos za sve proizvode u listi koja se nalazi na kartici.
    public Double getTotalFee() {
        ArrayList<FoodDomain> listFood = getListCart();
        double fee = 0;

        for(int i = 0;i<listFood.size();i++)
        {
            fee = fee + (listFood.get(i).getFee() * listFood.get(i).getNumberInCart());
        }
        return fee;
    }

    // Metoda "removeFromCart" briše sve proizvode sa kartice iz liste koja se čuva u lokalnoj memoriji i prikazuje Toast poruku o uklanjanju proizvoda sa kartice.
    public void removeFromCart() {
        tinyDB.clear();
        Toast.makeText(context, "Proizvodi su uklonjeni sa kartice", Toast.LENGTH_SHORT).show();
    }
}
