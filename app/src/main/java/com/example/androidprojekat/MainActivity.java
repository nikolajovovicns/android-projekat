package com.example.androidprojekat;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.androidprojekat.Adapter.CategoryAdapter;
import com.example.androidprojekat.Adapter.PopularAdapter;
import com.example.androidprojekat.Domain.CategoryDomain;
import com.example.androidprojekat.Domain.FoodDomain;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    // Deklarišemo dva Adapter objekta i dva RecyclerView objekta koji će se koristiti za prikazivanje liste kategorija i popularnih jela.
    // RecyclerView objekt služi za prikazivanje skupa podataka u listi, a Adapter objekt se koristi za povezivanje podataka sa RecyclerView-om.
    private RecyclerView.Adapter adapter, adapter2;
    private RecyclerView recyclerViewCategoryList, recyclerViewPopularList;


    //Metoda onCreate() je centralna metoda u aktivnosti koja se poziva prilikom kreiranja same aktivnosti.
    // Ova metoda se koristi za inicijalizaciju i postavljanje svih osnovnih elemenata aktivnosti.
    // U ovom slučaju, pozivamo tri metode: recyclerViewCategory(), recyclerViewPopular() i bottomNavigation(), koje se koriste za prikazivanje kategorija hrane i popularne hrane na glavnom ekranu, kao i za prikazivanje dugmadi za korpu i početni ekran.
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        recyclerViewCategory();
        recyclerViewPopular();
        bottomNavigation();
    }

    // Ova metoda se koristi za inicijalizaciju i postavljanje listenera na dugme za prikaz korpe sa porudžbinama i dugme za povratak na početni ekran.
    // Kada korisnik klikne na dugme za prikaz korpe, pokreće se nova aktivnost CartListActivity koja prikazuje sve porudžbine u korpi.
    // Kada korisnik klikne na dugme za povratak na početni ekran, pokreće se nova aktivnost MainActivity koja prikazuje sve kategorije hrane i popularne stavke.
    private void bottomNavigation(){
        FloatingActionButton floatingActionButton = findViewById(R.id.cartBtn);
        LinearLayout homeBtn = findViewById(R.id.homeBtn);

        floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MainActivity.this,CartListActivity.class));
            }
        });

        homeBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MainActivity.this, MainActivity.class));
            }
        });
    }

    // Ova metoda se koristi za inicijalizaciju RecyclerView-a koji prikazuje kategorije hrane.
    // Kreira se LinearLayoutManager objekat sa horizontalnom orijentacijom, a zatim se postavlja na RecyclerView objekat.
    // Zatim se kreira ArrayList objekat koji sadrži objekte tipa CategoryDomain koji predstavljaju kategorije hrane.
    // Svaki objekat kategorije sastoji se od naziva kategorije i ID-ja.
    // Nakon toga, kreira se adapter objekat i postavlja se na RecyclerView objekat.
    private void recyclerViewCategory() {
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL,false);
        recyclerViewCategoryList = findViewById(R.id.recyclerView);
        recyclerViewCategoryList.setLayoutManager(linearLayoutManager);

        ArrayList<CategoryDomain> category = new ArrayList<>();
        category.add(new CategoryDomain("Pice","cat_1"));
        category.add(new CategoryDomain("Burgeri","cat_2"));
        category.add(new CategoryDomain("Hotdogovi","cat_3"));
        category.add(new CategoryDomain("Pića","cat_4"));
        category.add(new CategoryDomain("Krofne","cat_5"));

        adapter = new CategoryAdapter(category);
        recyclerViewCategoryList.setAdapter(adapter);
    }

    // Ova metoda služi za popunjavanje RecyclerView-a popularnih jela.
    // Koristi se LinearLayoutManager da bi se postavila orijentacija RecyclerView-a kao horizontalna.
    // Zatim se kreira nova ArrayLista tipa FoodDomain koja sadrži tri popularna jela, svako sa svojim nazivom, opisom, slikom i cenom. Nakon toga se instancira objekat adaptera PopularAdapter i postavlja se kao adapter za RecyclerView.
    private void recyclerViewPopular() {
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this,LinearLayoutManager.HORIZONTAL, false);
        recyclerViewPopularList = findViewById(R.id.recyclerView2);
        recyclerViewPopularList.setLayoutManager(linearLayoutManager);

        ArrayList<FoodDomain> foodList = new ArrayList<>();
        foodList.add(new FoodDomain("Peperoni pica", "pop_1","Pelat, mocarela, šunka, kulen, masline, origano",3.22));
        foodList.add(new FoodDomain("Čizburger", "pop_2","Zemička, 100% govedina, Cheddar sir, senf, kiseli krastavac, kečap, dehidririani komadići luka",4.15));
        foodList.add(new FoodDomain("Pica sa povrćem", "pop_3","Paradajz, luk, brokoli, kukuruzni šećer, paprika, tikvice",6.55));

        adapter2 = new PopularAdapter(foodList);
        recyclerViewPopularList.setAdapter(adapter2);
    }
}

